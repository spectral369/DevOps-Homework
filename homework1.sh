#! /bin/bash
#Petru P.
#Github: github.com/spectral369
#Codeberg: codeberg.org/spectral369/DevOps-Homework01

#sudo necesar pentru instalare de pachete fara prompt
#if [ $EUID != 0 ]; then
#    sudo "$0" "$@"
#    exit $?
#fi

#vm capable
is_capable=$((`egrep -c '(vmx|svm)' /proc/cpuinfo`)) #$(()) -> trasform str to num
if [[ is_capable > 6 ]]; then
	echo "System is capable for virtualization: ${is_capable}"
else
	echo "System is not capable for virtualiation ${is_capable}"
	echo "Script exit..."
	exit 1 
fi

#package check
function package_exists() { #unix.stackexchange solution
    dpkg -s "$1" &> /dev/null
    return $?
}

if package_exists qemu-system-x86; then
	echo "qemu-system-x86 is installed !"
else
     echo 'Please install package : qemu-system-x86'
     #sudo apt-get install qemu-system-x86 #apt-get are suport pentru bash/ apt arunca un warning
     exit 1
fi
if package_exists libvirt-clients; then
	echo "libvirt-clients is installed !"
else
     echo 'Please install package : libvirt-clients'
      #sudo apt-get install libvirt-clients
     exit 1
fi
if package_exists libvirt-daemon-system; then
	echo "libvirt-daemon-system is installed !"
else
     echo 'Please install package : libvirt-daemon-system'
     #sudo apt-get install libvirt-daemon-system
     exit 1
fi
if package_exists bridge-utils; then
	echo "bridge-utils is installed !"
else
     echo 'Please install package : bridge-utils'
     #sudo apt-get install bridge-utils
     exit 1
fi
if package_exists virtinst; then
	echo "virtinst is installed !"
else
     echo 'Please install package : virtinst'
     #sudo apt-get install virtinst
     exit 1
fi
if package_exists libvirt-daemon; then
	echo "libvirt-daemon is installed !"
else
     echo 'Please install package : libvirt-daemon'
     #sudo apt-get install libvirt-daemon
     exit 1
fi
if package_exists wget; then
	echo "wget is installed !"
else
     echo 'Please install package : wget'
     #sudo apt-get install wget
     exit 1
fi

kvm_version=`kvm --version`
if [[ $kvm_version =~ ^[[:space:]]*[a-zA-Z]*[[:space:]]*[a-z]+*[[:space:]]*([a-z]+*[[:space:]]*[0-9\.]+) ]]; then 
	 echo ${BASH_REMATCH}
else
	echo "Unknown version Or Error - a.k.a teapa"
fi

#creating vm
#download debian11
`wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.5.0-amd64-netinst.iso -P $HOME/`
#create preseedFile
echo "#d-i debian-installer/locale string en_US
d-i debian-installer/language string en
d-i debian-installer/country string RO
d-i debian-installer/locale string en_US.UTF-8
d-i debian-installer/exit/poweroff boolean true

d-i keyboard-configuration/xkb-keymap select en

#d-i netcfg/get_hostname string debian1
d-i netcfg/get_hostname string debian
#d-i netcfg/hostname string debian
d-i netcfg/get_domain debian1

d-i passwd/root-password password debian1
d-i passwd/root-password-again password debian1
d-i passwd/user-fullname string debian
d-i passwd/username string debian1
d-i passwd/user-password password debian21
d-i passwd/user-password-again password debian21
d-i clock-setup/utc boolean true
d-i partman-md/confirm boolean true

d-i partman-auto/method string regular
d-i partman-auto/init_automatically_partition \\
   select Guided - use entire disk
d-i partman-auto/choose_recipe select All files in one partition (recommended for new users)
d-i partman/choose_partition select finish
d-i partman/confirm_nooverwrite boolean true


d-i apt-setup/cdrom/set-first boolean false

d-i mirror/country string RO
d-i mirror/http/mirror select deb.debian.org
d-i mirror/http/proxy string 

d-i popularity-contest/participate boolean false

d-i tasksel/first multiselect openssh-server


d-i grub-installer/with_other_os boolean true
d-i grub-installer/bootdev string /dev/vda

d-i finish-install/reboot_in_progress note
#test
d-i preseed/late_command string in-target mkdir -p /root/startup; \\
in-target wget https://codeberg.org/spectral369/DevOps-Homework01/raw/branch/main/startup.sh -P /root/startup; \\
in-target chown -R root:root /root/startup/; \\
in-target chmod +x /root/startup/startup.sh; \\
in-target /root/startup/startup.sh" > preseed.cfg


me_folder="${HOME}"
hd_path="${me_folder}/debian.qcow2"
echo `qemu-img create -f qcow $hd_path 5G`
echo 'done creating disk ! '
echo 'Creating vm...please wait '

virt-install --virt-type kvm --name debian --ram 2048 --disk $hd_path  --network network=default --accelerate --graphics none  --autoconsole text --noreboot --vcpus 4 --initrd-inject preseed.cfg --extra-args "ks=file:/preseed.cfg console=tty0 console=ttyS0,115200n8" --os-variant=debian11 --location=$HOME/debian-11.5.0-amd64-netinst.iso,kernel=install.amd/vmlinuz,initrd=install.amd/initrd.gz

#list vms and start the machine
virsh list --all
echo -e "\033[36mStarting VM this will take ~25 seconds..."
virsh start debian
sleep 20 #wait 20 seconds ?!!?
all_machines=`virsh list` #get vm id
vm_id=$(echo $all_machines | tr -dc '0-9')
ip_raw=`virsh domifaddr $vm_id`
ip_max_len=${#ip_raw}
ip_min_len=$(expr $ip_max_len - 18)
ip_ok=${ip_raw:ip_min_len:15}
echo $ip_ok
xdg-open http://$ip_ok

#finish gata :))
